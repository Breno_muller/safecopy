safecopy (1.7-7) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control: bumped Standards-Version to 4.6.1.
  * debian/copyright:
      - Converted the last paragraph of the GPL-2 and GPL-3 in a comment.
      - Split GPL license text into GPL-3 and GPL-3+.
      - Updated packaging copyright years.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 23 Nov 2022 23:10:12 -0300

safecopy (1.7-6) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control: bumped Standards-Version to 4.5.0.
  * debian/copyright: updated packaging copyright years.
  * debian/upstream/metadata: added new fields: Repository-Browse, Bug-Database
    and Bug-Submit.

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 07 Apr 2020 00:24:45 -0300

safecopy (1.7-5) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.4.1.
  * debian/copyright:
      - Added packaging rights for Samuel Henrique.
      - Updated packaging copyright years.

  [ Samuel Henrique ]
  * Add salsa-ci.yml.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 06 Jan 2020 18:56:20 -0300

safecopy (1.7-4) unstable; urgency=medium

  * debian/copyright:
      - Added rights for Raphaël Hertzog and SZ Lin (林上智).
      - Updated packaging copyright years.
  * debian/rules: removed '--with autoreconf' because it is default since DH 10.
  * debian/tests/*: added to perform a trivial test.
  * debian/watch: using a secure URI.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 22 Nov 2018 08:49:08 -0200

safecopy (1.7-3) unstable; urgency=medium

  * Team upload.
  [ Raphaël Hertzog ]
  * d/control:
    - Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org
    - Update team maintainer address to Debian Security Tools

  [ SZ Lin (林上智) ]
  * Add upstream metadata file
  * d/control:
    - Remove dh-autoreconf for Build-Depends: since it is enabled by default now
    - Bump debhelper version to 11
    - Bump Standards-Version to 4.2.1
  * d/copyright:
    - Replace "http" with "https" in URL
  * d/compat:
    - Bump compat version to 11

 -- SZ Lin (林上智) <szlin@debian.org>  Sat, 01 Sep 2018 22:55:41 +0800

safecopy (1.7-2) unstable; urgency=medium

  * Bumped DH level to 10.
  * debian/control:
      - Bumped Standards-Version to 3.9.8.
      - Updated the Vcs-* fields to use https instead of http and git.
  * debian/copyright:
      - Added a Comment field to explain about the upstream's nickname.
      - Removed an outdated upstream email address.
      - Updated the packaging copyright years.
  * debian/patches/fix-manpage: renamed to 10_fix-manpage.patch.
  * debian/watch: bumped to version 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 17 Nov 2016 19:01:23 -0200

safecopy (1.7-1) unstable; urgency=medium

  * New upstream release. (Closes: #718732, LP: #1148388)
  * New co-maintainer.
  * Migrations:
      - debian/changelog to 1.0 format.
      - DebSrc to 3.0 format.
      - DH level to 9.
      - Using dh-autoreconf.
  * Removed the manpage from debian/ because the upstream is providing
      an updated manpage.
  * Removed the direct changes in upstream source code. No patch needed
      because the upstream fixed the code. Thanks!
  * debian/control:
      - Bumped Standards-Version to 3.9.6.
      - Improved the short and long descriptions.
      - Updated the Vcs-* fields.
  * debian/copyright: full updated.
  * debian/patches/fix-manpage: created to fix some issues in manpage.
  * debian/README.Debian: added to list some documentation places.
  * debian/rules:
      - Added DEB_BUILD_MAINT_OPTIONS to improve the GCC hardening.
      - Removed some unnecessary lines.
  * debian/safecopy.docs:
      - Renamed to docs.
      - Added the specification.txt file.
  * debian/source.lintian-overrides: removed. Using 'Team upload' now.
  * debian/watch: fixed and improved.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 25 Jun 2015 08:58:06 -0300

safecopy (1.6-1) unstable; urgency=low

  [ Christophe Monniez ]
  * Merging upstream version 1.6.

 -- Michael Prokop <mika@debian.org>  Thu, 05 Nov 2009 00:40:59 +0100

safecopy (1.5-1) unstable; urgency=low

  [ Daniel Baumann ]
  * Minimizing rules file.
  * Adding stuff in rules that doesn't get done by dh automatically.

  [ Christophe Monniez ]
  * Merging upstream version 1.5.
  * Filling out the man page (Closes: #541339).

  [ Michael Prokop ]
  * Update to Standards-Version 3.8.3 (no further changes).
  * Update VCS-* headers (moved to alioth).
  * Bump build dependency on debhelper to >= 7.0.50~.

 -- Michael Prokop <mika@debian.org>  Fri, 25 Sep 2009 01:18:18 +0200

safecopy (1.3-2) unstable; urgency=low

  * Updating package to standards version 3.8.2.
  * Adding lintian overrides.

 -- Daniel Baumann <daniel@debian.org>  Thu, 09 Jul 2009 12:35:49 +0200

safecopy (1.3-1) unstable; urgency=low

  * Merging upstream version 1.3.

 -- Daniel Baumann <daniel@debian.org>  Thu, 28 May 2009 23:21:36 +0200

safecopy (0.2-2) unstable; urgency=low

  * Adding myself to copyright.
  * Improving description, thanks to Loic Minier <lool@dooz.org>
    (Closes: #520096, #520208).
  * Tidy rules file.
  * Updating to standards 3.8.1.

 -- Daniel Baumann <daniel@debian.org>  Thu, 28 May 2009 22:27:36 +0200

safecopy (0.2-1) unstable; urgency=low

  [ Juan Angulo Moreno ]
  * Initial release (Closes: #512208).

  [ Daniel Baumann ]
  * Removing useless whitespaces at EOF and EOL.
  * Updating vcs fields in control.
  * Rewrapping package long description to 80 chars a line in control.
  * Correcting copyright file.
  * Removing useless dirs debhelper file.
  * Removing AUTHORS from docs, doesn't contain new information over copyright
    file.
  * Removing NEWS from docs, doesn't contain new information over ChangeLog.
  * Reordering rules file.
  * Moving out manpage of debian directory to subdirectory.
  * Prefixing debhelper files with package name.

 -- Daniel Baumann <daniel@debian.org>  Tue, 20 Jan 2009 21:22:26 +0100
